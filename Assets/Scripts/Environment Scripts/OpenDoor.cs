using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour, Interactable
{
    public AudioClip sound;
    AudioSource audioSource;
    public float openSpeed = 15.0f;
    protected bool isOpened = false;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Interact()
    {
        Debug.Log("Interact() called!");
        if (!isOpened)
        {
            audioSource.PlayOneShot(sound, 0.7f);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, 90, 0)), Time.deltaTime * openSpeed);
            //isOpened = true;
        }
    }
}
