using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rift : MonoBehaviour
{
    public GameObject exit;
    public float riftPositionOffsetY = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        other.transform.position = exit.transform.position + (exit.transform.up * riftPositionOffsetY);
        other.transform.eulerAngles = new Vector3(
            other.transform.eulerAngles.x,
            exit.transform.eulerAngles.y,
            other.transform.eulerAngles.z);
    }
}
