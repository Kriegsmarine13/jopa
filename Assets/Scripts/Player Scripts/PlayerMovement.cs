using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{ 
    public GameObject detectionRadius;
    public float initialDetectionRadius = 5.0f;
    public float sprintDetectionRadiusModifier = 2.5f;
    public float sneakDetectionRadiusModifier = 0.5f;
    private bool detectVisible = false;

    public float moveSpeed = 10.0f;
    private float initialMoveSpeed = 10.0f;
    public float rotationSpeed = 75.0f;

    private Collider collidedObject;

    public float stamina = 100.0f;
    public float maxStamina = 100.0f;
    public float staminaUsage = 0.05f;
    public float staminaRegenRate = 0.01f;
    public float sprintSpeed = 17.5f;
    public float minStaminaToSprint = 17.5f;
    private bool sprintPause = false;

    public float sneakSpeed = 5.0f;
    public bool isSneaking = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // DEBUG ZONE
        if(Input.GetKeyDown(KeyCode.Y))
        {
            detectionRadius.SetActive(detectVisible);
            detectVisible = !detectVisible;
        }
        // END OF DEBUG ZONE

        // Movement
        if (Input.GetKey("w"))
        {
            transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * moveSpeed;
        }

        if (Input.GetKey("s"))
        {
            transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * -moveSpeed;
        }

        // Rotation
        if (Input.GetKey("a"))
        {
            transform.Rotate(0, -rotationSpeed * Time.deltaTime, 0);
        }

        if (Input.GetKey("d"))
        {
            transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
        }

        if (Input.GetKey("e") && collidedObject != null)
        {
            collidedObject.GetComponent<Interactable>().Interact();
        }

        // Sprinting
        if(stamina > minStaminaToSprint)
        {
            sprintPause = false;
        }

        if (Input.GetKey(KeyCode.LeftShift) && stamina > 0.0f && !sprintPause)
        {
            moveSpeed = sprintSpeed;
            stamina -= staminaUsage;
            ChangeDetectionRadius(initialDetectionRadius * sprintDetectionRadiusModifier);
        } else
        {
            if (stamina < maxStamina)
            {
                stamina += staminaRegenRate;
            }
        }

        if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            moveSpeed = initialMoveSpeed;
            ChangeDetectionRadius(initialDetectionRadius);
            if (stamina < minStaminaToSprint)
            {
                sprintPause = true;
            }
        }

        // Crouching / Sneaking
        if(Input.GetKey(KeyCode.LeftCommand))
        {
            Debug.Log("Keypressed");
            if(!isSneaking)
            {
                moveSpeed = sneakSpeed;
                isSneaking = true;
                ChangeDetectionRadius(initialDetectionRadius * sneakDetectionRadiusModifier);
            }
        }

        if(Input.GetKeyUp(KeyCode.LeftCommand))
        {
            isSneaking = false;
            moveSpeed = initialMoveSpeed;
            ChangeDetectionRadius(initialDetectionRadius);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Interactable")
        {
            collidedObject = other;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        collidedObject = null;
    }

    private void OnCollisionEnter(Collision collision)
    {
        
    }

    public void ChangeDetectionRadius(float radius)
    {
        detectionRadius.transform.localScale = new Vector3(radius, radius, radius);
    }
}
